﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace DepthLibrary
{
    public class Map
    {
        List<Tuple<Double, Double, Double>> points = new List<Tuple<Double, Double, Double>>();

        public Map()
        {
            using (var reader = new StreamReader(@"C:\Users\Mahmood\Desktop\Depth\depth.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    points.Add(new Tuple<Double, Double, Double>(Convert.ToDouble(values[0]), Convert.ToDouble(values[1]), Convert.ToDouble(values[2])));
                }
            }
        }

        public Double getDepth(Double lat, Double lng) {
            Double latLowerBound = roundDown(lat);
            Double latUpperBound = roundUp(lat);
            Double lngLowerBound = roundDown(lng);
            Double lngUpperBound = roundUp(lng);

            Double minimumLatBound = latLowerBound;
            Double minimumLngBound = lngLowerBound;
            Double minimumDistance = calculateDistance(lat, lng, latLowerBound, lngLowerBound);

            Double tmpDistance = calculateDistance(lat, lng, latLowerBound, lngUpperBound);
            if (tmpDistance < minimumDistance)
            {
                minimumDistance = tmpDistance;
                minimumLatBound = latLowerBound;
                minimumLngBound = lngUpperBound;
            }

            tmpDistance = calculateDistance(lat, lng, latUpperBound, lngLowerBound);
            if (tmpDistance < minimumDistance)
            {
                minimumDistance = tmpDistance;
                minimumLatBound = latUpperBound;
                minimumLngBound = lngLowerBound;
            }

            tmpDistance = calculateDistance(lat, lng, latUpperBound, lngUpperBound);
            if (tmpDistance < minimumDistance)
            {
                minimumDistance = tmpDistance;
                minimumLatBound = latUpperBound;
                minimumLngBound = lngUpperBound;
            }

            return fetchDepth(minimumLatBound, minimumLngBound);
        }


        private Double calculateDistance(Double lat1, Double lng1, Double lat2, Double lng2)
        {
            return Math.Sqrt(Math.Pow(lat2 - lat1, 2) + Math.Pow(lng2 - lng1, 2));
        }

        private Double fetchDepth(Double lat, Double lng)
        {
            foreach (var point in this.points)
            {
                if (point.Item1 == lat && point.Item2 == lng)
                {
                    return point.Item3;
                }
            }
            return -888888;
        }

        public Double roundUp(Double input)
        {
            return Math.Ceiling(input * 100) / 100;
        }

        public Double roundDown(Double input)
        {
            return Math.Floor(input * 100) / 100;
        }
    }
}
