﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DepthLibrary;
using System.Drawing;
using Newtonsoft.Json;
using System.IO;
using System.Collections;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Map map = new DepthLibrary.Map();
            // Double result = map.getDepth(24.27921231, 57.6031231);
            // Console.WriteLine(result);
            // Console.ReadKey();

            MapGeometry.Line line = new MapGeometry.Line(new PointF((float)26.516621, (float)54.050243), new PointF((float)26.571754, (float)53.997182));

            var kishPolygonPoints = new List<PointF>();
            using (var reader = new StreamReader(@"C:\Users\Mahmood\Desktop\Depth\kish-points-all.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var csvLine = reader.ReadLine();
                    var values = csvLine.Split(',');

                    PointF tmp = new PointF(float.Parse(values[0]), float.Parse(values[1]));
                    kishPolygonPoints.Add(tmp);
                }
            }

            MapGeometry.Polygon polygon = new MapGeometry.Polygon(kishPolygonPoints.ToArray());

            var watch = System.Diagnostics.Stopwatch.StartNew();

            var res = MapGeometry.Geometry.IntersectionOf(line, polygon);

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            // Console.WriteLine(elapsedMs);

            Console.WriteLine(res);
            Console.ReadKey();
        }
    }
}
